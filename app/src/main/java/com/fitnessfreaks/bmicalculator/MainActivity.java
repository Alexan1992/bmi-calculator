package com.fitnessfreaks.bmicalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    //Declaration of class variables or "fields".
    TextView resultText;
    RadioButton radioButtonMale, radioButtonFemale;
    EditText ageEditText, feetEditText, inchesEditText, weightEditText;
    Button buttonCalculate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Calling the methods necessary to run the app. First, we need to find the views from the XML
        findViews();
        //Then we implement the click listener, so upon clicking the BMI calculation works and displayed to the user.
        setupClickListener();
    }
    //Method that contains the views from the xml and assign them to variables.
    private void findViews(){
        resultText = findViewById(R.id.text_view_result);
        radioButtonMale = findViewById(R.id.radio_button_male);
        radioButtonFemale = findViewById(R.id.radio_button_female);
        ageEditText = findViewById(R.id.edit_text_age);
        feetEditText = findViewById(R.id.edit_text_feet);
        inchesEditText = findViewById(R.id.edit_text_inches);
        weightEditText = findViewById(R.id.edit_text_weight);
        buttonCalculate = findViewById(R.id.button_calculate);
    }
    //In this method the button click listener has been set up
    private void setupClickListener() {
        buttonCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Upon clicking this method is triggered.
                double bmiResult = calculateBMI();

                String ageText = ageEditText.getText().toString();
                int age = Integer.parseInt(ageText);

                if (age >= 18){
                    displayResult(bmiResult);
                } else {
                    displayGuidance(bmiResult);
                }
            }
        });
    }
    //In this method all the BMI calculation happens
    private double calculateBMI() {
        //Assign the values provided by user in the layout to string variables
        String feetText = feetEditText.getText().toString();
        String inchesText = inchesEditText.getText().toString();
        String weightText = weightEditText.getText().toString();

        //Converting strings to integers
        int feet = Integer.parseInt(feetText);
        int inches = Integer.parseInt(inchesText);
        int weight = Integer.parseInt(weightText);

        //Converting feet to inches and adding to the inches provided by the user
        int totalInches = (feet * 12) + inches;

        //Converting inches to meters
        double heightInMeters = totalInches * 0.0254;

        //Calculate the BMI
        return weight / (heightInMeters * heightInMeters);
    }

    private void displayResult(double bmi){
        DecimalFormat myDecimalFormatter = new DecimalFormat("0.00");
        String bmiTextResult = myDecimalFormatter.format(bmi);

        //Display the BMI result to the screen
        String fullResultString;
        if (bmi < 18.5){
            fullResultString = bmiTextResult + "- You are underweight";
        } else if (bmi > 25){
            fullResultString = bmiTextResult + "- You are overweight";
        } else {
            fullResultString = bmiTextResult + "- You are healthy weight";
        }
        resultText.setText(fullResultString);
    }

    private void displayGuidance(double bmi) {
        DecimalFormat myDecimalFormatter = new DecimalFormat("0.00");
        String bmiTextResult = myDecimalFormatter.format(bmi);

        String fullResultString;

        if (radioButtonMale.isChecked()){
            //Display boy guidance
            fullResultString = bmiTextResult + " - As you are under 18, please consult with your doctor for healthy range for boys";
        } else if (radioButtonFemale.isChecked()){
            //Display girl guidance
            fullResultString = bmiTextResult + " - As you are under 18, please consult with your doctor for healthy range for girls";
        } else {
            //Display general guidance
            fullResultString = bmiTextResult + " - As you are under 18, please consult with your doctor for healthy range";
        }

        resultText.setText(fullResultString);
    }
}